<div class="card">
  <div class="card-body">
    <div class="text-center">
      <center>
      <div class="card-title">
        <h1 class>GESTION DE VACUNAS</h1><br>
      </div>
        <button type="button" name="button" class="btn btn-primary bnt-xl" onclick="cargarListadoVacunas();" title="Actualizar">
          <i class="fa fa-refresh"></i>
        </button>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-success btn-xl" data-toggle="modal" data-target="#modalNuevaVacuna">
          <i class="fa fa-plus-circle"></i>  Agregar Vacuna
        </button>
      </center>
      <br>
      <div class="row">
        <div class="col-md-12" id="contenedor-listado-vacunas">
        <i class="fa fa-spin fa-xl fa-spinner"> </i>
          Consultando Datos
        </div>
      </div>
    </div>
  </div>
</div>


 <!-- Modal -->
 <div id="modalNuevaVacuna"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <i class="fa-solid fa-syringe"></i>
         <h4 class="modal-title"><i class="fa-solid fa-syringe"></i> Nueva Vacuna</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
         <form class="" action="<?php echo site_url('vacunas/insertarVacuna'); ?>" method="post" id="frm_nueva_vacuna">
           <label for="">NOMBRE:</label><br>
           <select class="form-control" name="nombre_vac" id="nombre_vac">
             <option value="">--- Selecione una Opción ---</option>
             <option value="Pfizer">Pfizer</option>
             <option value="AstraZeneca">AstraZeneca</option>
             <option value="Sinovac">Sinovac</option>
           </select>
           <br><br>
            <label for="">ESTADO</label><br>
           <select class="form-control" name="estado_vac" id="estado_vac">
             <option value="">--- Selecione una Opción ---</option>
             <option value="ACTIVO">ACTIVO</option>
             <option value="INACTIVO">INACTIVO</option>
           </select>
           <br>
           <label for="">Centro de Salud:</label><br>
           <input type="text" name="tipo_vac" id="tipo_vac" class="form-control"> <br>
           <button type="submit" name="button" class="btn btn-success">
             <br>

             <br>
             <i class="fa fa-save"></i> Guardar
           </button>
         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>



  <!-- Modal -->
  <div id="modalEditarVacuna"
  style="z-index:9999 !important;" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-content"> <i class="fa-solid fa-syringe"></i> Editar Vacuna</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="contenedor-formulario-editar">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>



<script type="text/javascript">
    function cargarListadoVacunas(){
      $("#contenedor-listado-vacunas").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-listado-vacunas").load("<?php echo site_url(); ?>/vacunas/listado");
    }
    cargarListadoVacunas();
    $("#frm_nueva_vacuna").validate({
      rules:{
        nombre_vac:{
          required:true,
        },
        tipo_vac:{
          required:true
        }
      },
      messages:{
        nombre_vac:{
          required:"Por favor ingrese el nombre",
        },
        tipo_vac:{
          required:"Por favor ingrese el tipo",
        }
      },
      submitHandler:function(form){//funcion para peticiones AJAX
          $.ajax({
            url:$(form).prop("action"),
            type:"post",
            data:$(form).serialize(),
            success:function(data){
                cargarListadoVacunas();
                $("#modalNuevaVacuna").modal("hide");
                $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                $('.modal-backdrop').remove();//eliminamos el backdrop del modal
                var objetoJson=JSON.parse(data);
                if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="ok"){
                  iziToast.success({
                       title: 'CONFIRMACIÓN',
                       message: 'Inserción Exitosa',
                       position: 'topRight',
                     });
                }else{
                  iziToast.error({
                       title: 'ERROR',
                       message: 'Error al procesar',
                       position: 'topRight',
                     });
                }
            }
          });
      }
    });

</script>


<script type="text/javascript">
    function abrirFormularioEditar(id_vac){
      // alert("ok...");
      $("#contenedor-formulario-editar").html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-formulario-editar").load("<?php echo site_url('vacunas/editar'); ?>/"+id_vac);
      $("#modalEditarVacuna").modal("show");
    }
</script>
