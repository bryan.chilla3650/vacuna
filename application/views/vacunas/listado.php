<?php if ($listadoVacunas): ?>
  <table class="table table-bordered table-striped table-hover">
      <thead>
         <tr>
           <th class="text-center">ID</th>
           <th class="text-center">NOMBRE</th>
           <th class="text-center">NOMBRE CENTRO DE SALUD</th>
           <th class="text-center">ESTADO</th>
           <th class="text-center">OPCIONES</th>
         </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoVacunas->result() as $vacuna): ?>
              <tr>
                  <td class="text-center">
                    <?php echo $vacuna->id_vac; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $vacuna->nombre_vac; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $vacuna->tipo_vac; ?>
                  </td>
                  <td class="text-center">
                    <?php if ($vacuna->estado_vac): ?>
                        <div class="alert alert-success">
                          ACTIVO
                        </div>
                      <?php else: ?>
                        <div class="alert alert-danger">
                          INACTIVO          
                        </div>
                      <?php endif; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" onclick="abrirFormularioEditar(<?php echo $vacuna->id_vac; ?>);" class="btn btn-warning" title="Editar">
                      <i class="fa fa-edit"></i>
                    </a>
                    &nbsp;
                    <a href="#" class="btn btn-danger" onclick="eliminarVacuna(<?php echo $vacuna->id_vac; ?>);" title="Eliminar">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
<?php else: ?>
    <br>
    <div class="alert alert-danger">
        No se encontraron vacunas Registradas
    </div>
<?php endif; ?>
<script type="text/javascript">
  function eliminarVacuna(id_vac){

    iziToast.question({
    timeout: 20000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'Hey',
    message: 'Desea Eliminar este registro de forma permanente?',
    position: 'center',
    buttons: [
                ['<button><b>Si</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    $.ajax({
                        url:"<?php echo site_url('vacunas/eliminarVacuna'); ?>",
                        type:"post",
                        data:{"id_vac":id_vac},
                        success:function(data){
                          cargarListadoVacunas();
                          alert(data);
                        }
                    });

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ],
            onClosing: function(instance, toast, closedBy){
                console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                console.info('Closed | closedBy: ' + closedBy);
            }
        });


  }
</script>
