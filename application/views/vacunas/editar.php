<form class="" action="<?php echo site_url('vacunas/actualizarVacunaAjax'); ?>" method="post" id="frm_actualizar_vacuna">
    <input type="hidden" name="id_vac" id="id_vac" value="<?php echo $vacuna->id_vac; ?>">
    <label for="">NOMBRE:</label><br>
    <select class="form-control" name="nombre_vac" id="nombre_vac_editar">
      <option value="">--- Selecione una Opción ---</option>
      <option value="Pfizer">Pfizer</option>
      <option value="AstraZeneca">AstraZeneca</option>
      <option value="Sinovac">Sinovac</option>
    </select>
    <label for="">TIPO:</label><br>
    <input type="text" name="tipo_vac" value="<?php echo $vacuna->tipo_vac; ?>" id="tipo_vac_editar" class="form-control"> <br>
    <label for="">ESTADO:</label><br>
    <select class="form-control" name="estado_vac" id="estado_vac_editar">
     <option value="">--- Seleccione una opción ---</option>
     <option value="1">ACTIVO</option>
     <option value="0">INACTIVO</option>
    </select><br>
    <button type="button" onclick="actualizar();" name="button" class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
</form>

<script type="text/javascript">
  $("#nombre_vac_editar").val("<?php echo $vacuna->nombre_vac; ?>");
  $("#estado_vac_editar").val("<?php echo $vacuna->estado_vac; ?>");
</script>

<script type="text/javascript">
function actualizar(){
    $.ajax({
      url:$("#frm_actualizar_vacuna").prop("action"),
      type:"post",
      data:$("#frm_actualizar_vacuna").serialize(),
      success:function(data){
        cargarListadoVacunas();
        $("#modalEditarVacunas").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script>
