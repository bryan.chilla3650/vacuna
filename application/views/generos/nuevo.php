<form action="<?php echo site_url(); ?>/generos/guardarGenero" method="post" id="frm_nuevo_genero">
    <div class="col-md-12" >
<div class="row">
    <div class="col-md-4" ></div>
        <div class="col-md-8">
          <br>
          <center>
          <h1> <b>FORMULARIO DE REGISTRO DE GENEROS</b>  </h1>
          <br>
          </center>
          <br>
            <center>
            <label for=""><b>GENERO</b></label><br>
            <select class="form-control" type="text" name="tipo_gen" id="tipo_gen">
                <option>SELECCIONE</option>
                <option>MASCULINO</option>
                <option>FEMENINO</option>
                </select><br>


                <button class="btn btn-info " type="submit" name="button">REGISTRAR</button>
                &nbsp; &nbsp;&nbsp
                <a href="<?php echo site_url(); ?>/generos/index" class="btn btn-warning">CANCELAR</a>
              </center>
            </div>
        <div class="col-md-2"></div>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        $("#frm_nuevo_genero").validate({
          rules:{
            titulo_man:{
              letras:true,
              required:true
            },
            autor_man:{
              required:true


            },
          editorial_man:{

              required:true
            },
            idioma_man:{
              required:true


            },
          precio_man:{

              required:true,
              digits:true
            },
            estado_man:{

              required:true
            }
          },
          messages:{
            titulo_man:{
              required:"Por favor ingrese el titulo"
            },
          autor_man:{
              required:"Por favor ingrese el autor",

            },
            editorial_man:{
                required:"Por favor ingrese la editorial",

              },
            idioma_man:{
                  required:"Por favor ingrese el idioma",

                },
            precio_man:{
              required:"Porfavor ingrese el precio",
              digits:"El precio solo acepta números"

            },

            estado_man:{
              required:"Porfavor ingrese su estado"
            }

          }
        });
    </script>
