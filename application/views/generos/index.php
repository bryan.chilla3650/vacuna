    <div class="col-md-12">
              <br>
              <center>
                  <h2><b>Lista de Genero</b></h2>
                  <br>
                  <a href="<?php echo site_url(); ?>/generos/nuevo" class="btn btn-info"> <i class="fa fa-plus"></i> Agregar Genero </a>
              </center>
              <br>

                  <?php if ($listadoGenero): ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover responsive" id="tbl-generos">
                            <thead>
                              <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">NOMBRE:</th>
                                <th class="text-center">OPCIONES</th>
                              </tr>
                            </thead>

                            <tbody>
                              <?php foreach ($listadoGenero->result() as $filaTemporal): ?>
                                  <tr>
                                      <td class="text-center">
                                          <?php echo $filaTemporal->id_gen; ?>
                                      <td class="text-center">
                                          <?php echo $filaTemporal->tipo_gen; ?>
                                      </td>
                                      <td class="text-center">
                                          <a href="<?php echo site_url() ?>/generos/editar/<?php echo $filaTemporal->id_gen ?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
                                          <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_gen; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
                                      </td>
                                  </tr>
                              <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                  <?php else: ?>
                        <div class="alert alert-danger">
                            <h3>No se encontraron genero resgitrados</h3>
                        </div>
                  <?php endif; ?>
    </div>


<script type="text/javascript">
    function confirmarEliminacion(id_gen){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el genero de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/generos/procesarEliminacion/"+id_gen;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>


<script type="text/javascript">
  $(document).ready( function () {
  	$('#tbl-generos').DataTable({
      dom: 'Blfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
      "language":idioma_español
    });
  } );
  </script>
