<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-md-12">
        <div class="text-center">
          <div class="card-title">
            <h2> <b>Nueva Dosis</b> </h2><br>
          </div>
        </div>
          <form class="" action="<?php echo site_url(); ?>/dosis/guardarDosis" method="post" id="frm_nuevo">
            <div class="row">
              <div class="col-md-6">
                <label for="">Seleccione el Paciente:</label><br>
                <select class="form-control" name="fk_id_per" id="fk_id_per">
                  <option value="">--- Seleccione uno ---</option>
                  <?php if ($listadoPersona): ?>
                    <?php foreach ($listadopersona->result() as $filaTemporal): ?>
                      <option value="<?php echo $filaTemporal->id_per ?>">
                        <?php echo $filaTemporal->id_per ?>
                      </option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select><br>
                <label for="">Seleccione la vacuna:</label><br>
                <select class="form-control" name="fk_id_vac" id="fk_id_vac">
                  <option value="">--- Seleccione uno ---</option>
                  <?php if ($listado): ?>
                    <?php foreach ($listado->result() as $vacunaTemporal): ?>
                      <option value="<?php echo $vacunaTemporal->id_vac ?>">
                        <?php echo $vacunaTemporal->nombre_vac ?>
                      </option>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </select><br>
                <label for="">Lugar de vacunacion:</label><br>
                <input type="text" name="lugar_dos" id="lugar_dos" placeholder="Ingrese el centro de vacunacion" class="form-control"><br>
              </div>
              <div class="col-md-6">
                <label for="">Fecha:</label><br>
                <input type="date" name="fecha_dos" id="fecha_dos" class="form-control" value=""><br>
                <label for="">Numero de dosis</label><br>
                <select class="form-control" name="numero_dos" id="numero_dos">
                  <option value="">--- Seleccione uno ---</option>
                  <option value="1">Primera</option>
                  <option value="2">Segunda</option>
                  <option value="3">Tercera</option>
                  <option value="4">Cuarta</option>
                </select><br>
                <label for="">Nombre del vacunador </label><br>
                <input type="text" name="vacunador_dos" id="vacunador_dos" class="form-control"value="">
              </div>
            </div>
            <button type="submit" class="btn btn-info" name="button"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
            &nbsp;&nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/dosis/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
          </form>
      </div>
    </div>
  </div>
</div>
