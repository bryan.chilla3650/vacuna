<?php
    class Vacuna extends CI_Model{
      public function __construct(){
        parent::__construct();
      }


      public function insertarVacuna($data){
          return $this->db->insert("vacuna",$data);
      }

      public function obtenerTodos(){
        // $this->db->order_by("apellido_usu","asc");
        $listado=$this->db->get("vacuna");
        if($listado->num_rows()>0){
          return $listado;
        }else{
          return false;
        }
      }
      public function obtenerPorId($id_vac){
        $this->db->where("id_vac",$id_vac);
        $vacuna=$this->db->get("vacuna");
        if($vacuna->num_rows()>0){
          return $vacuna->row();
        }else{
          return false;
        }
      }
      public function eliminar($id_vac){
          $this->db->where("id_vac",$id_vac);
          return $this->db->delete("vacuna");
      }
      public function actualizar($data, $id_vac){
          $this->db->where("id_vac",$id_vac);
          return $this->db->update("id_vac",$data);
      }
    }//cierre de la clase Usuario
