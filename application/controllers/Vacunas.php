<?php
      class Vacunas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("vacuna");
            //validando si alguien esta conectado
            // if ($this->session->userdata("c0nectadoUTC")) {
            //   // si esta conectado
            //   if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="1")
            //   {
            //     // SI ES ADMINISTRADOR
            //   } else {
            //     redirect("/");
            //   }
            // } else {
            //   redirect("seguridades/formularioLogin");
            // }
        }

      public function index(){
        $this->load->view("header");
        $this->load->view("vacunas/index");
        $this->load->view("footer");
      }
      //funcion que rendizara la vista listado.php
      public function listado(){
        $data["listadoVacunas"]=$this->vacuna->obtenerTodos();
        $this->load->view("Vacunas/listado",$data);
      }
      //Insercion asincrona
      public function insertarVacuna(){
        $data=array(
           "nombre_vac"=>$this->input->post("nombre_vac"),
           "tipo_vac"=>$this->input->post("tipo_vac"),
           "estado_vac"=>$this->input->post("estado_vac"),
           );
        if($this->vacuna->insertarVacuna($data)){
           echo json_encode(array("respuesta"=>"ok"));
        }else{
          echo json_encode(array("respuesta"=>"error"));
        }
      }
      //Eliminacion a traves de AJAX
      public function eliminarVacuna(){
          $id_vac=$this->input->post("id_vac");
          if($this->vacuna->eliminar($id_vac)){
            echo json_encode(array("respuesta"=>"ok"));
          }else{
            echo json_encode(array("respuesta"=>"error"));
          }
      }

      public function editar($id_vac){
        $data["vacuna"]=$this->vacuna->obtenerPorId($id_vac);
        $this->load->view("vacunas/editar",$data);
      }

      public function actualizarVacunaAjax(){
          $id_vac=$this->input->post("id_vac");
          $data=array(
            "nombre_vac"=>$this->input->post("nombre_vac"),
            "tipo_vac"=>$this->input->post("tipo_vac"),
            "estado_vac"=>$this->input->post("estado_vac"),
          );
          if($this->vacuna->actualizar($data,$id_vac)){
              echo json_encode(array("respuesta"=>"ok"));
          }else{
              echo json_encode(array("respuesta"=>"error"));
          }
      }


}//cierre de la clase
