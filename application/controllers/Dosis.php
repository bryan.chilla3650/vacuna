<?php
  class Dosis extends CI_Controller{
     public function __construct(){
      parent::__construct();
      $this->load->model("dosi");
      $this->load->model("vacuna");
    }

    public function index(){
      $data["listadoDosis"]=$this->dosi->consultarTodos();
      $this->load->view("header");
      $this->load->view("dosis/index",$data);
      $this->load->view("footer");
    }

    public function nuevo(){
      $data["listado"]=$this->vacuna->obtenerTodos();
      // $data["listadoPersonas"]=$this->persona->consultarTodos();
      $this->load->view("header");
      $this->load->view("dosis/nuevo",$data);
      $this->load->view("footer");
    }

    public function editar($id_dos){
      $data["listado"]=$this->vacuna->obtenerTodos();
      // $data["listadoPersonas"]=$this->persona->consultarTodos();
      $data["dosis"]=$this->dosi->consultarPorId($id_dos);
      $this->load->view("header");
      $this->load->view("dosis/editar",$data);
      $this->load->view("footer");
    }

    public function procesarActualizacion(){
      $id_dos=$this->input->post("id_dos");
      $datosDosisEditado=array(
        "lugar_dos"=>$this->input->post("lugar_dos"),
        "fecha_dos"=>$this->input->post("fecha_dos"),
        "numero_dos"=>$this->input->post("numero_dos"),
        "fk_id_vac"=>$this->input->post("fk_id_vac"),
        "fk_id_per"=>$this->input->post("fk_id_per")
      );
      if ($this->dosi->actualizar($id_dos,$datosDosisEditado)) {
        $this->session->set_flashdata("confirmacion","dosis actualizada correctamente");
      } else {
        $this->session->set_flashdata("Error","Error al procesar, inetente nuevamente");
      }
      redirect("dosis/index");
    }

    public function guardarDosis(){
      $datosNuevaDosis=array(
        "lugar_dos"=>$this->input->post("lugar_dos"),
        "fecha_dos"=>$this->input->post("fecha_dos"),
        "numero_dos"=>$this->input->post("numero_dos"),
        
        "fk_id_vac"=>$this->input->post("fk_id_vac"),
        // "fk_id_per"=>$this->input->post("fk_id_per")
      );
      if ($this->dosi->insertar($datosNuevaDosis)) {
        $this->session->set_flashdata("confirmacion","dosis insertada correctamente");
      } else {
        $this->session->set_flashdata("Error","Error al procesar, inetente nuevamente");
      }
      redirect("dosis/index");
    }

    public function procesarEliminacion($id_dos){
      if ($this->dosi->eliminar($id_dos)) {
        $this->session->set_flashdata("confirmacion","Dosis eliminada exitosamente.");
      } else {
        $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
      }
      redirect("dosis/index");
    }
  }//cierre funcion
?>
