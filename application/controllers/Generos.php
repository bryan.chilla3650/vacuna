<?php
    class Generos extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('genero');
          
        }

        public function index(){
          $data["listadoGenero"]=$this->genero->consultarTodos();
          $this->load->view('header');
          $this->load->view('generos/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $this->load->view('header');
          $this->load->view('generos/nuevo');
          $this->load->view('footer');
        }
        public function editar($id_gen){
          $data['genero']=$this->genero->consultarPorId($id_gen);
          $this->load->view('header');
          $this->load->view('generos/editar',$data);
          $this->load->view('footer');
        }
          public function procesarActualizacion(){
            $id_gen = $this->input->post("id_gen");
            $datosGeneroEditado=array(
                  "tipo_gen"=>$this->input->post("tipo_gen")

            );

            if ($this->genero->actualizar($id_gen,$datosGeneroEditado)) {

                //Pongan esto para que al momento de editar salga el mensaje flash
                $this->session->set_flashdata('confirmacion','MANGA EDITADO EXITOSAMENTE');
              } else {
                $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
              }
              redirect("generos/index");
              }

        //registro de usuarios
        public function guardarGenero(){
          $datosNuevoGenero=array(
            "tipo_gen"=>$this->input->post("tipo_gen")

          );

          if ($this->genero->insertar($datosNuevoGenero)){
              $this->session->set_flashdata("confirmacion","MANGA INSERTADAO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("generos/index");

        }
        public function procesarEliminacion($id_gen){
            if($this->genero->eliminar($id_gen)){
                redirect("generos/index");
            }else{
                echo "ERROR AL ELIMINAR";
            }
        }
    }//cierre de la clase
 ?>
