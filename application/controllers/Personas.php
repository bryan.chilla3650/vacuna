<?php
    class Personas extends CI_Controller{
      public function __construct(){
        parent::__construct();
        //MODELO persona
        $this->load->model("persona");

        // //validando si alguien esta conectado ESTO ES LO PRIMORDIAL
        // if ($this->session->userdata("c0nectadoUTC")) {
        //   // si esta conectado
        //   if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR")
        //   {
        //     // SI ES ADMINISTRADOR
        //   } else {
        //     redirect("/");
        //   }
        // } else {
        //   redirect("seguridades/formularioLogin");
        // }
      }
      public function index(){
        $data["listadoPersonas"]=$this->persona->consultarTodos();
         $this->load->view("header");
         $this->load->view("personas/index",$data);
         $this->load->view("footer");

      }
      public function nuevo(){
        $data["listadoPersonas"]=$this->persona->consultarTodos();
        $this->load->view("header");
        $this->load->view("personas/nuevo",$data);
        $this->load->view("footer");
      }

      public function editar($id_per){
        $data['persona']=$this->persona->consultarPorId($id_per);
        $this->load->view("header");
        $this->load->view("personas/editar",$data);
        $this->load->view("footer");
      }
//para actualizar ahorita clase del jueves

public function procesarActualizacion(){
$id_per = $this->input->post("id_per");
$datosPersonaEditado=array(
  "cedula_per"=>$this->input->post("cedula_per"),
            "nombre_per"=>$this->input->post("nombre_per"),
            "apellido_per"=>$this->input->post("apellido_per"),
            "telefono_per"=>$this->input->post("telefono_per"),
            "email_per"=>$this->input->post("email_per"),


);

//logica de negocio necesarioa para subir la fotografia del persona

$this->load->library('upload');//carga de la libreria de subida de archivos
$nombreTemporal='foto_persona_'.time().'_'.rand(1,500);//generar nombre aleatorio
$config['file_name']=$nombreTemporal; //te,poral
$config['upload_path']=APPPATH.'../uploads/personas/'; //ruta de la subida de los archivos
$config['allowed_types']='jpeg|jpg|png';//tipo de datos permitidos
$config['max_size']=4*1024;//configurar el peso maximo de los archivos
$this->upload->initialize($config);//inicializar la configuracion
//instruccion para que se valide la subida del archivos
if($this->upload->do_upload('foto_per')){
  //FUNCION PARA QUE SE ME ACTUALICE LA FOTO CUANDO PONGO UNA NUEVA FOTO
$query =$this->persona->obtenerPorId($id_per);
unlink(APPPATH.'../uploads/personas/'.$query->foto_per);


$dataSubida=$this->upload->data();
$datosPersonaEditado['foto_per']=$dataSubida['file_name'];

}

if ($this->persona->actualizar($id_per,$datosPersonaEditado)) {

            //Pongan esto para que al momento de editar salga el mensaje flash
            $this->session->set_flashdata('confirmacion','PERSONA EDITADA EXITOSAMENTE');
          } else {
            $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          }
          redirect("personas/index");
          }

//guardar persona en el controlador personas - agregar un
//dato en el controlador para que se guarde los archivos
      public function guardarpersona(){
        $datosNuevopersona=array(
                  "cedula_per"=>$this->input->post("cedula_per"),
                    "nombre_per"=>$this->input->post("nombre_per"),
                    "apellido_per"=>$this->input->post("apellido_per"),
                    "telefono_per"=>$this->input->post("telefono_per"),
                    "email_per"=>$this->input->post("email_per"),
        );
        //logica de negocio necesarioa para subir la fotografia del persona
        $this->load->library('upload');//carga de la libreria de subida de archivos
        $nombreTemporal='foto_persona_'.time().'_'.rand(1,500);//generar nombre aleatorio
        $config['file_name']=$nombreTemporal; //te,poral
        $config['upload_path']=APPPATH.'../uploads/personas/'; //ruta de la subida de los archivos
        $config['allowed_types']='jpeg|jpg|png';//tipo de datos permitidos
        $config['max_size']=5*1024;//configurar el peso maximo de los archivos
        $this->upload->initialize($config);//inicializar la configuracion
//instruccion para que se valide la subida del archivos
        if($this->upload->do_upload('foto_per')){
          $dataSubida=$this->upload->data();
          $datosNuevopersona['foto_per']=$dataSubida['file_name'];

        }

        if($this->persona->insertar($datosNuevopersona)){
          //echo "INSERCION EXITOSA";
          //crear sesion flash
          $this->session->set_flashdata('confirmacion','persona insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("personas/index");
      }

      public function procesarEliminacion($id_per){

        $data['persona']=$this->persona->obtenerPorId($id_per);
        $id_img= $data['persona']->foto_per;
        unlink(  APPPATH.'../uploads/personas/'.$id_img);


            if($this->persona->eliminar($id_per)){
                redirect("personas/index");
      }
          else{
                echo "ERROR AL ELIMINAR";
              }

      }
    }//cierre de la clase


 ?>
